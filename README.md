# Samba Domain Controler

**O que faz**

Este script faz o download da ultima versão do samba para compilação do domain controler.

**Como uSar**

Basta vc fazer o clone do projeto e editar o arquivo em group_vars/all  e mudar as variaveis e pronto.

**Exemplo**
```
hostname_server: "Samba-Server-001"
domain_server: "brsecurity.com.br"
netbios_domain: "brsecurity"
samba_admin_password: Mandacaga23
dir_install_samba: "/opt/samba-{{ samba_version }}"
```


# Instalando o rsat


```
 Add-WindowsCapability -Name Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0 -Online
```  


Abaixo uma descrição dos aplicativos rsat e suas descriçoes.


| Função | Descrição | Nome |
| --- | --- | --- |
| Active Directory Domain Services and Lightweight Directory Services Tools | As ferramentas do AD DS (Serviços de Domínio Active Directory) e do AD LDS (Active Directory Lightweight Directory Services) incluem ferramentas de linha de comando e snap-ins para gerenciar remotamente o AD DS e o AD LDS no Windows Server. | Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0 |
| Utilitários de Administração de Criptografia de Unidade de Disco BitLocker | Os Utilitários de Administração da Criptografia de Unidade de Disco BitLocker incluem ferramentas para gerenciar os recursos da Criptografia de Unidade de Disco BitLocker. O Visualizador de Senha de Recuperação do Active Directory do BitLocker ajuda a localizar as senhas de recuperação da Criptografia de Unidade de Disco BitLocker no AD DS (Active Directory Domain Services). | Rsat.BitLocker.Recovery.Tools~~~~0.0.1.0 |
| Ferramentas de Serviços de Certificados do Active Directory | As Ferramentas de Serviços de Certificados do Active Directory incluem os snap-ins de Autoridade de Certificação, Modelos de Certificado, PKI corporativa e Gerenciamento de Respondente Online para gerenciar remotamente o AD CS no Windows Server | Rsat.CertificateServices.Tools~~~~0.0.1.0 |
| Ferrametas de Servidor DHCP | As Ferramentas de Servidor DHCP incluem o snap-in DHCP do MMC, contexto de netsh de servidor DHCP e um módulo do Windows PowerShell para Servidor DHCP. | Rsat.DHCP.Tools~~~~0.0.1.0 |
| Ferrametas de Servidor DNS | As Ferramentas do Servidor DNS incluem o snap-in Gerenciador DNS, a ferramenta de linha de comando dnscmd.exe e o módulo do Windows PowerShell para Servidor DNS. | Rsat.Dns.Tools~~~~0.0.1.0 |
| Ferrametas de Servidor de Serviços de Arquivo | As Ferramentas de Serviços de Arquivo incluem snap-ins e ferramentas de linha de comando para o gerenciamento remoto da função Serviços de Arquivo no Windows Server. | Rsat.FileServices.Tools~~~~0.0.1.0 |
| Ferramentas de Cluster de Failover | As Ferramentas de Clustering de Failover incluem o snap-in Gerenciador de Cluster de Failover, a interface Atualização com Suporte a Cluster e o módulo Cluster de Failover para Windows PowerShell. | Rsat.FailoverCluster.Management.Tools~~~~0.0.1.0 |
| Ferramentas de Gerenciamento de Política de Grupo | As Ferramentas de Gerenciamento de Política de Grupo incluem o Console de Gerenciamento de Política de Grupo, Editor de Gerenciamento de Política de Grupo e o Editor de GPO de Início da Política de Grupo. | Rsat.GroupPolicy.Management.Tools~~~~0.0.1.0 |
| Cliente IPAM ( Gerenciamento de Endereços IP) | O Cliente de Gerenciamento de Endereço IP (IPAM) é usado para se conectar ao servidor IPAM remoto e para gerenciá-lo. O IPAM oferece uma estrutura central para o gerenciamento de espaço de endereçamento IP e de servidores de infraestrutura correspondentes, como DHCP e DNS em uma floresta do Active Directory. | Rsat.IPAM.Client.Tools~~~~0.0.1.0 |
| Ferramentas do Data Center Bridging LLDP | As ferramentas LLDP de Ponte de Data Center incluem ferramentas do PowerShell para gerenciar remotamente agentes LLDP no Windows Server. | Rsat.LLDP.Tools~~~~0.0.1.0 |
| Ferramentas de Gerenciamento de Controlador de Rede | As Ferramentas de Gerenciamento de Controlador de Rede incluem ferramentas do PowerShell para gerenciar a função Controlador de Rede no Windows Server. | Rsat.NetworkController.Tools~~~~0.0.1.0 |
| Ferramentas de Balanceamento de Carga de Rede | As Ferramentas de Balanceamento de Carga de Rede incluem o snap-in Gerenciador de Balanceamento de Carga de Rede, o módulo Balanceamento de Carga de Rede para Windows PowerShell e as ferramentas de linha de comando nlb.exe e wlbs.exe. | Rsat.NetworkLoadBalancing.Tools~~~~0.0.1.0 |
| Ferramentas de Gerenciamento de Acesso Remoto | As Ferramentas de Gerenciamento de Acesso Remoto incluem ferramentas gráficas e do PowerShell para gerenciar a função Acesso Remoto no Windows Server. | Rsat.RemoteAccess.Management.Tools~~~~0.0.1.0 |
| Ferramentas de Serviços de Área de Trabalho Remota | As Ferramentas de Serviços de Área de Trabalho Remota incluem snap-ins para o Gerenciador de Licenciamento de Área de Trabalho Remota, o Diagnóstico de Licenciamento de Área de Trabalho Remota e o Gerenciador de Gateway de Área de Trabalho Remota. Use o Gerenciador do Servidor para administrar todos os outros serviços de função RDS. | Rsat.RemoteDesktop.Services.Tools~~~~0.0.1.0 |
| Gerenciador do Servidor | O Gerenciador do Servidor inclui o console do Gerenciador do Servidor e as ferramentas do PowerShell para gerenciar remotamente o Windows Server e inclui ferramentas para configurar o agrupamento NIC no Windows Server e o Analisador de Práticas Recomendadas. | Rsat.ServerManager.Tools~~~~0.0.1.0 |
| Ferramentas de Gerenciamento de Serviço de Migração de Armazenamento | Fornece ferramentas de gerenciamento para trabalhos de migração de armazenamento. | Rsat.StorageMigrationService.Management.Tools~~~~0.0.1.0 |
| Módulo de Réplica de Armazenamento para Windows Powershell | Inclui o módulo do PowerShell para gerenciar remotamente o recurso Réplica de Armazenamento no Windows Server 2016 e posterior. | Rsat.StorageReplica.Tools~~~~0.0.1.0 |
| Módulo de Insights do Sistema para Windows Powershell | O Módulo de Insights do Sistema para Windows PowerShell permite gerenciar o recurso Insights do Sistema. | Rsat.SystemInsights.Management.Tools~~~~0.0.1.0 |
| Ferramentas de Ativação de Volume | As Ferramentas de Ativação de Volume podem ser usadas para gerenciar chaves de licença de ativação de volume em um host KMS (Serviço de  Gerenciamento de Chaves) ou nos Serviços de Domínio do Microsoft Active Directory. Essas ferramentas podem ser usadas para instalar, ativar e gerenciar uma ou mais chaves de licença de ativação de volume, bem como para definir as configurações do KMS no Windows Server. | Rsat.VolumeActivation.Tools~~~~0.0.1.0 |
| Ferramentas para Windows Server Update Services | As Ferramentas para Windows Server Update Services incluem ferramentas gráficas e do PowerShell para o gerenciamento do WSUS. | Rsat.WSUS.Tools~~~~0.0.1.0 |
| Ferramentas da VM Blindadas **Somente Windows 10 | As Ferramentas da VM Blindadas incluem o Assistente de Arquivo de Dados de Provisionamento e o Assistente de Disco do Modelo. | Rsat.Shielded.VM.Tools~~~~0.0.1.0 |
| Módulo do Powershell para Azure Stack HCI **Somente Windows 11 | O módulo PowerShell para o Azure Stack HCI permite gerenciar o Azure Stack HCI. | Rsat.AzureStack.HCI.Management.Tools~~~~0.0.1.0 |  
___



  
